<?php

namespace Drupal\config_patch_gitlab_api\Gitlab;

use Gitlab\Api\MergeRequests;
use Gitlab\Client;
use Gitlab\ResultPager;

/**
 * Class ConfigPatchGitlabClient.
 *
 * @package Drupal\config_patch_gitlab_api\Gitlab
 */
class ConfigPatchGitlabClient {

  /**
   * @var \Gitlab\Client
   */
  private $client;

  /**
   * ConfigPatchGitlabClient constructor.
   *
   * @param \Gitlab\Client $client
   */
  public function __construct(Client $client) {
    $this->client = $client;
  }

  /**
   * Gets the projects that meet the required conditions from parameters.
   *
   * @param array $parameters
   *   Array of parameters for project search.
   *   See more here: https://docs.gitlab.com/ee/api/projects.html#list-all-projects.
   *
   * @return array
   *   The array of projects with properties as array.
   */
  public function getProjects(array $parameters = []): array {
    return $this->client->projects()->all($parameters);
  }

  /**
   * Gets the project by Id.
   *
   * @param int $id
   *   Id of the project.
   *
   * @return array
   */
  public function getProjectById($id) {
    return $this->client->projects()->show($id);
  }

  /**
   * Get branches per project.
   *
   * @param int $project_id
   *   Id of the project.
   *
   * @return array
   * @throws \Http\Client\Exception
   */
  public function getBranches(int $project_id): array {
    $pager = new ResultPager($this->client);
    return $pager->fetchAll($this->client->repositories(), 'branches', [$project_id, ['per_page' => 100]]);
  }

  /**
   * @param int $project_id
   * @param $branch_name
   *
   * @return array|NULL
   */
  public function getBranch(int $project_id, $branch_name) {
    foreach ($this->getBranches($project_id) as $branch) {
      if ($branch['name'] === $branch_name) {
        return $branch;
      }
    }
    return NULL;
  }


  /**
   * Get the list of commits from the branch.
   *
   * @param int $project_id
   *   Id of the project.
   * @param string $branch_name
   *   Branch name.
   *
   * @return array
   */
  public function getCommits(int $project_id, string $branch_name): array {
    return $this->client->repositories()->commits($project_id, [
      'ref_name' => $branch_name,
    ]);
  }

  /**
   * Get the difference between the branch and latest commit.
   *
   * @param int $project_id
   *   Id of the project.
   * @param string $branch_name
   *   Branch name.
   *
   * @return string
   */
  public function diff(int $project_id, string $branch_name) {
    $commits = $this->getCommits($project_id, $branch_name);
    $latest_commit = $commits[0];
    return $this->client->repositories()->diff($project_id, $latest_commit['id']);
  }

  /**
   * Create the commit in GitLab repository.
   *
   * @param int $project_id
   *   Id of the project.
   * @param string $start_branch
   *   The start branch from which the new one with the commit will be created.
   * @param string $branch_name
   *   The name of the new branch.
   * @param array $actions
   *   The list of files that were changed. Read more here https://docs.gitlab.com/ee/api/commits.html#create-a-commit-with-multiple-files-and-actions.
   * @param string $message
   *   Commit message (required by Gitlab).
   *
   * @return mixed
   */
  public function createCommit(int $project_id, string $start_branch, string $branch_name, array $actions, string $message = '') {
    $params = [
      'branch' => $branch_name,
      'commit_message' => $message,
      'actions' => $actions,
      'author_name' => 'Drupal Config Patch module,',
    ];
    // If the branch doesn't exist, specify start branch.
    if (!$this->getBranch($project_id, $branch_name)) {
      $params['start_branch'] = $start_branch;
    }
      return $this->client->repositories()->createCommit($project_id, $params);
  }

  /**
   * Create merge request in Gitlab.
   *
   * @param int $project_id
   *   Id of the project.
   * @param string $target_branch
   *   The branch that will be merged.
   * @param string $branch_name
   *   The branch where the changes from $start_branch will be merged.
   * @param string $title
   *   The title of Merge Request.
   *
   * @return mixed
   */
  public function createMergeRequest(int $project_id, string $target_branch, string $branch_name, string $title) {
    return $this->client->mergeRequests()->create($project_id, $target_branch, $branch_name, $title);
  }

  /**
   * Get opened merge requests.
   *
   * @param int $project_id
   *   Id of the project.
   * @param string $target_branch
   *   The branch that will be merged.
   * @param string $branch_name
   *   The branch where the changes from $start_branch will be merged.
   *
   * @return mixed
   */
  public function getOpenMergeRequest($project_id, $target_branch, $branch_name) {
    return $this->client->mergeRequests()->all($project_id, ['source_branch' => $branch_name, 'target_branch' => $target_branch, 'state' => MergeRequests::STATE_OPENED]);
  }

  /**
   * Get repository tree.
   *
   * @param int $project_id
   *   Id of the project.
   * @param string $branch_name
   *   The branch where the changes from $start_branch will be merged.
   *
   * @return mixed
   * @throws \Http\Client\Exception
   */
  public function getTree(int $project_id, string $branch_name, $path) {
    $pager = new ResultPager($this->client);
    return $pager->fetchAll($this->client->repositories(), 'tree', [$project_id, ['ref' => $branch_name, 'path' => $path, 'per_page' => 100]]);
  }

  /**
   * Shows the version of Gitlab API.
   *
   * @return mixed
   *   The version of Gitlab API.
   */
  public function version() {
    return $this->client->version()->show();
  }

}
