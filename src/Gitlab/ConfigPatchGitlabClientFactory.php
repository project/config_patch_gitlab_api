<?php declare(strict_types=1);

namespace Drupal\config_patch_gitlab_api\Gitlab;

use Drupal\config_patch_gitlab_api\Exception\MissingCredentialsException;
use Drupal\config_patch_gitlab_api\Form\ConfigPatchGitlabApiCredentialsForm;
use Drupal\Core\State\StateInterface;
use Gitlab\Client;

class ConfigPatchGitlabClientFactory {

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  private $state;

  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  public function create(array $settings = []): ConfigPatchGitlabClient {
    $settings = empty($settings) ? $this->getSavedSettings() : $settings;
    $gitlab_client = new Client();
    $gitlab_client->setUrl($settings['url']);
    $gitlab_client->authenticate($settings['token'], Client::AUTH_HTTP_TOKEN);
    return new ConfigPatchGitlabClient($gitlab_client);
  }

  private function getSavedSettings(): array {
    $settings = $this->state->get(ConfigPatchGitlabApiCredentialsForm::SETTINGS, []);
    $this->validateSettings($settings);
    return $settings;
  }

  private function validateSettings(array $settings): void {
    if (empty($settings['url'])) {
      throw new MissingCredentialsException('The Gitlab url is not defined.');
    }
    if (empty($settings['token'])) {
      throw new MissingCredentialsException('The Gitlab token is not defined.');
    }
  }

}
