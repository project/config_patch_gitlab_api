<?php


namespace Drupal\config_patch_gitlab_api\Controller;


use Drupal\Component\Utility\Xss;
use Drupal\config_patch_gitlab_api\Gitlab\ConfigPatchGitlabClientFactory;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProjectsAutoCompleteController
 *
 * @package Drupal\config_patch_gitlab_api\Controller
 */
class ProjectsAutoCompleteController extends ControllerBase {

  /**
   * @var \Drupal\config_patch_gitlab_api\Gitlab\ConfigPatchGitlabClient
   */
  protected $client;

  /**
   * ProjectsAutoCompleteController constructor.
   *
   * @param \Drupal\config_patch_gitlab_api\Gitlab\ConfigPatchGitlabClientFactory $clientFactory
   */
  public function __construct(ConfigPatchGitlabClientFactory $clientFactory) {
    $this->client = $clientFactory->create();
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\Core\Controller\ControllerBase|static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config_patch_gitlab_api.client_factory')
    );
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function handleAutocomplete(Request $request) {
    $results = [];
    $input = $request->query->get('q');
    // Get the typed string from the URL, if it exists.
    if (!$input) {
      return new JsonResponse($results);
    }
    $input = Xss::filter($input);
    $projects = $this->client->getProjects(['search' => $input]);
    foreach ($projects as $project) {
      $results[] = [
        'value' => $project['name_with_namespace'] . ' (' . $project['id'] . ')',
        'label' => $project['name_with_namespace'],
      ];
    }
    return new JsonResponse($results);
  }

}
