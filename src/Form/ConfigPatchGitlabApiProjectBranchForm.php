<?php

namespace Drupal\config_patch_gitlab_api\Form;

use Composer\Semver\Semver;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\config_patch_gitlab_api\Exception\MissingCredentialsException;
use Drupal\config_patch_gitlab_api\Gitlab\ConfigPatchGitlabClient;
use Drupal\config_patch_gitlab_api\Gitlab\ConfigPatchGitlabClientFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\State\StateInterface;
use Gitlab\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigPatchGitlabApiProjectBranchForm
 *
 * @package Drupal\config_patch_gitlab_api\Form
 */
class ConfigPatchGitlabApiProjectBranchForm extends FormBase {

  /**
   * Config patch settings state name.
   */
  const SETTINGS = 'config_patch_gitlab_api.project_branch';

  /**
   * @var \Drupal\config_patch_gitlab_api\Gitlab\ConfigPatchGitlabClientFactory
   */
  protected ConfigPatchGitlabClientFactory $configPatchGitlabClientFactory;

  /**
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected UrlGeneratorInterface $urlGenerator;

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * ConfigPatchGitlabApiProjectBranchForm constructor.
   *
   * @param \Drupal\config_patch_gitlab_api\Gitlab\ConfigPatchGitlabClientFactory $config_patch_gitlab_client_factory
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   * @param \Drupal\Core\State\StateInterface $state
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   */
  public function __construct(
    ConfigPatchGitlabClientFactory $config_patch_gitlab_client_factory,
    UrlGeneratorInterface $url_generator,
    StateInterface $state,
    CacheBackendInterface $cacheBackend
  ) {
    $this->configPatchGitlabClientFactory = $config_patch_gitlab_client_factory;
    $this->urlGenerator = $url_generator;
    $this->state = $state;
    $this->cache = $cacheBackend;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_patch_gitlab_api_project_branch';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config_patch_gitlab_api.client_factory'),
      $container->get('url_generator'),
      $container->get('state'),
      $container->get('cache.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    try {
      $client = $this->configPatchGitlabClientFactory->create();
    }
    catch (MissingCredentialsException $e) {
      return $this->getMissingCredentialsResponse($e);
    }
    if (empty($form_state->getUserInput())) {
      $settings = $this->state->get(static::SETTINGS, []);
    } else {
      $settings = $form_state->getUserInput();
      $settings['project_id'] = static::extractProjectIdFromAutocompleteInput($settings['project_id']);
    }
    $current_project = NULL;
    if (!empty($settings['project_id']) && is_numeric($settings['project_id'])) {
      $current_project = $client->getProjectById($settings['project_id']);
      if ($current_project) {
        $current_project = $current_project['name_with_namespace'] . ' (' . $settings['project_id'] . ')';
      } else {
        $current_project = $settings['project_id'];
      }
    }
    $wrapper_id = Html::getUniqueId('ajax-refresh');
    $form['settings'] = [
      '#type' => 'container',
      '#id' => $wrapper_id,
    ];
    $form['settings']['project_id'] = [
      '#required' => TRUE,
      '#type' => 'textfield',
      '#title' => $this->t('Project'),
      '#autocomplete_route_name' => 'config_patch_gitlab_api.autocomplete.projects',
      '#default_value' => $current_project,
      '#ajax' => [
        'event' => 'autocompleteclose',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading project branches ...'),
        ],
        'callback' => [$this, 'ajaxRefresh'],
        'wrapper' => $wrapper_id,
        // Starting from Drupal 10.3 it should be 'replaceWith'.
        'method' => !Semver::satisfies(\Drupal::VERSION, '>=10.3.0') ? 'replace' : 'replaceWith',
      ],
    ];

    $options = $this->getBranchOptions($client, $form_state, $settings);

    $form['settings']['branch_name'] = [
      '#required' => TRUE,
      '#title' => $this->t('Target branch name'),
      '#description' => $this->t('This is the branch against which new merge requests will be created.'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => array_key_exists('branch_name', $settings) && in_array($settings['branch_name'], array_keys($options)) ? $settings['branch_name'] : '',
      '#states' => [
        'invisible' => [
          'select[name="project_id"]' => ['value' => ''],
        ],
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
    ];
    return $form;
  }

  /**
   * Returns the field value on ajax callback event change.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function ajaxRefresh(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $parents = $trigger['#array_parents'];
    $triggered_element = array_pop($parents);
    return NestedArray::getValue($form, $parents);
  }

  /**
   * Extracts the project ID from the autocompletion result.
   *
   * @param string $input
   *   The input coming from the autocompletion result.
   *
   * @return mixed|null
   *   An entity ID or NULL if the input does not contain one.
   */
  public static function extractProjectIdFromAutocompleteInput($input) {
    $match = NULL;

    // Take "label (entity id)', match the ID from inside the parentheses.
    // @todo Add support for entities containing parentheses in their ID.
    // @see https://www.drupal.org/node/2520416
    if (preg_match("/.+\s\(([^\)]+)\)/", $input, $matches)) {
      $match = $matches[1];
    }

    return $match;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Extracts the entity ID from the autocompletion result.
    $project_id = static::extractProjectIdFromAutocompleteInput($form_state->getValue('project_id'));
    $this->state->set(static::SETTINGS, [
      'project_id' => $project_id,
      'branch_name'  => $form_state->getValue('branch_name'),
    ]);
    $this->messenger()->addStatus($this->t('The configuration options have been saved.'));
  }

  /**
   * @param \Drupal\config_patch_gitlab_api\Exception\MissingCredentialsException $e
   *
   * @return array
   */
  private function getMissingCredentialsResponse(MissingCredentialsException $e): array {
    $link = $this->urlGenerator->generateFromRoute('config_patch_gitlab_api.credentials');
    $message = $this->t($e->getMessage() . ' Please define it <a href="@link">here</a>.', [
      '@link' => $link
    ]);
    $this->messenger()->addWarning($message);
    return [];
  }

  /**
   * Gets the list of branches in the project.
   *
   * @param \Drupal\config_patch_gitlab_api\Gitlab\ConfigPatchGitlabClient $client
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param array $settings
   *
   * @return array
   */
  private function getBranchOptions(
    ConfigPatchGitlabClient $client,
    FormStateInterface $form_state,
    array $settings
  ) {
    $project_id = empty($settings) ? $form_state->getValue('project_id') : $settings['project_id'];
    if (!$project_id) {
      return ['' => $this->t('Invalid project')];
    }
    $options = ['' => $this->t('-- Select branch --')];
    try {
      foreach($client->getBranches((int) $project_id) as $branch) {
        $options[$branch['name']] = ucfirst($branch['name']);
      }
      if (empty($options)) {
        $options[''] = $this->t('Project has no branches');
      }
    } catch (RuntimeException $e) {
      $this->messenger()->addError($e->getMessage());
    }
    return $options;
  }

}
