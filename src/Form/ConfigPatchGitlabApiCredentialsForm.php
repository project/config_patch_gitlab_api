<?php

namespace Drupal\config_patch_gitlab_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigPatchGitlabApiCredentialsForm.
 *
 * @package Drupal\config_patch_gitlab_api\Form
 */
class ConfigPatchGitlabApiCredentialsForm extends ConfigFormBase {

  /**
   * The name of state variable.
   */
  const SETTINGS = 'config_patch_gitlab_api.credentials';

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The config patch gitlab client factory.
   *
   * @var \Drupal\config_patch_gitlab_api\Gitlab\ConfigPatchGitlabClientFactory
   */
  protected $configPatchGitlabClientFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->state = $container->get('state');
    $instance->configPatchGitlabClientFactory = $container->get('config_patch_gitlab_api.client_factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_patch_gitlab_api_credentials';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->state->get(static::SETTINGS);
    $form['url'] = [
      '#type' => 'url',
      '#required' => TRUE,
      '#title' => $this->t('Gitlab URL'),
      '#description' => $this->t('E.g., https://[instance url]'),
      '#default_value' => $settings['url'] ?? '',
    ];

    $form['token'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('GitLab Project Access Token'),
      '#description' => $this->t('Create a project access token with api and write_repository scopes in GitLab. Visit https://[instance url]/[group]/[project]/-/settings/access_tokens.'),
      '#default_value' => $settings['token'] ?? '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $token = $form_state->getValue('token');
    if (strlen($token) < 20) {
      $form_state->setErrorByName('token', $this->t('The token is too short.'));
    }

    if (!$form_state->hasAnyErrors()) {
      try {
        $client = $this->configPatchGitlabClientFactory->create([
          'url' => $form_state->getValue('url'),
          'token' => $form_state->getValue('token'),
        ]);
        // Do a simple call for a version show to check the credentials.
        $client->version();
      }
      catch (\InvalidArgumentException $e) {
        $form_state->setErrorByName('', "The url and/or token are invalid.");
      }
      catch (\Throwable $e) {
        $form_state->setErrorByName('token', $e->getMessage());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->state->set(static::SETTINGS, [
      'url' => $form_state->getValue('url'),
      'token' => $form_state->getValue('token'),
    ]);
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

}
