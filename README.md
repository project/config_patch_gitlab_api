# Config Patch GitLab API



# Setup
1. Visit /admin/config/development/config_patch.
   a. Set "Config base path." E.g., `config/default`.
   b. Set default output plugin to "Merge requestion in Gitlab with API"
2. Create a new GitLab project access token 
   a. In your GitLab instance, visit https://[instance_url]/[group name]/[project id]/-/settings/access_tokens
   b. Name the Token "Config Patch".
   c. Grant`api` and `write_repository` scopes.
   d. Copy the token to your clipboard.
3. Visit /admin/config/config_patch_gitlab_api/credentials
   a. Enter your instance URL.
   b. Paste in project access token.
4. Visit /admin/config/config_patch_gitlab_api/project_branch
   a. Select your repository.
   b. Select the target branch for new Config Patch merge requests.

# Usage
Try exporting config!

### UI Patch Export
Visit /admin/config/development/configuration/patch/config_patch_gitlab_api 

### CLI Patch Export
`drush config:patch config_patch_gitlab_api --message="Exporting config from environment."`