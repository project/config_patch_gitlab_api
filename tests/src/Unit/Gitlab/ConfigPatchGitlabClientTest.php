<?php declare(strict_types=1);

namespace Drupal\Tests\config_patch_gitlab_api\Unit\Gitlab;

use Drupal\config_patch_gitlab_api\Gitlab\ConfigPatchGitlabClient;
use Drupal\Tests\UnitTestCase;
use Gitlab\Api\Projects;
use Gitlab\Api\Repositories;
use Gitlab\Client;

class ConfigPatchGitlabClientTest extends UnitTestCase {

  /**
   * @var \Drupal\config_patch_gitlab_api\Gitlab\ConfigPatchGitlabClient
   */
  private $unit;

  /**
   * @var \Gitlab\Client|\Prophecy\Prophecy\ObjectProphecy
   */
  private $client;

  public function testGetProjects() {
    /** @var \Gitlab\Api\Projects|\Prophecy\Prophecy\ObjectProphecy $projects */
    $projects = $this->prophesize(Projects::class);
    $projects->all()->willReturn(['projects']);

    $this->client->projects()->willReturn($projects->reveal());

    $actual = $this->unit->getProjects();
    $this->assertSame(['projects'], $actual);
  }

  public function testGetBranches() {
    $project_id = 1;
    $expected = ['branches'];
    /** @var \Gitlab\Api\Repositories|\Prophecy\Prophecy\ObjectProphecy $repositories */
    $repositories = $this->prophesize(Repositories::class);
    $repositories->branches($project_id)->willReturn($expected);
    $this->client->repositories()->willReturn($repositories->reveal());

    $actual = $this->unit->getBranches($project_id);
    $this->assertSame($expected, $actual);
  }

  public function setUp() {
    parent::setUp();
    $this->client = $this->prophesize(Client::class);
    $this->unit = new ConfigPatchGitlabClient(
      $this->client->reveal()
    );
  }

}
